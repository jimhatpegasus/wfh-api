const express = require('express');
const router = express.Router();

const imageDataURI = require('image-data-uri');

const app = require('../app');

const { SOCKET_PORT, ALLOW_ORIGIN } = require('../config');

const http = require('http');
const server = http.createServer(app);
const io = require('socket.io')(server, {origins: '*:*'});

io.on('connection', (client) => {
    client.on('app/SUBSCRIBE_TO_SOCKET', () => {
		console.log('{ ' + client.handshake.headers.referer, 'subscribed to socket server }');
    });
    client.on('disconnect', () => {
        console.log('{ ' + client.handshake.headers.referer, 'disconnected from socket server }');
    });
});

server.listen(SOCKET_PORT);

router.use(function(req, res, next) {
	res.header("Access-Control-Allow-Origin", ALLOW_ORIGIN);
	res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,PATCH,DELETE');
	next();
});

/* POST new delegate */
router.post('/create', function(req, res, next) {
    const delegate = {
        'name' : req.body.name,
        'last' : req.body.last,
        'email' : req.body.email,
        'message' : req.body.message,
        'approved' : false,
        'rejected' : false,
        'pending' : true,
        'contactConsent' : req.body.contactConsent,
		'projectConsent' : req.body.projectConsent,
        'photoConsent' : req.body.photoConsent,
        'signature' : req.body.signature
    };
    connection.query('INSERT INTO delegates SET ?, confirmedDate = NOW()', delegate, function (error, results) {
        if(error){
            res.send(JSON.stringify({"status": 500, "error": error, "response": null}));
        } else {
            res.send(JSON.stringify({"status": 200, "error": null, "response": results}));

            const uri = req.body.file;
            const id = results.insertId;
            const file = `user${id}_${new Date().toISOString().match(/[^:]*/)[0]}.png`;
            const path = `./public/images/${file}`;

			imageDataURI.outputFile(uri, path)
				.then(res => {
					connection.query(`UPDATE delegates SET file = '${file}' WHERE ID = ${id}`, function (error, results, fields) {
						if(error){
							io.of('/').emit('app/IMAGE_ERROR', {
								type: 'app/IMAGE_ERROR',
								id: id,
								error: error
							});
						} else {
							io.of('/').emit('app/IMAGE_READY', {
                                type: 'app/IMAGE_READY',
								id: id,
								file: file
							});
						}
						connection.end();
					});
				})
				.catch(error => {
					io.of('/').emit('app/IMAGE_ERROR', {
						id: results.insertId,
						error: error
					});
				});
        }
    });
});

/* GET all delegates */
router.get('/', function(req, res, next) {
	connection.query('SELECT * from delegates', function (error, results, fields) {
	  	if(error){
	  		res.send(JSON.stringify({"status": 500, "error": error, "response": null})); 
	  	} else {
  			res.send(JSON.stringify({"status": 200, "error": null, "response": results}));
	  	}
        connection.end();
  	});
});

/* GET all approved delegates */
router.get('/approved', function(req, res, next) {
	connection.query('SELECT * from delegates WHERE approved = true', function (error, results, fields) {
	  	if(error){
	  		res.send(JSON.stringify({"status": 500, "error": error, "response": null})); 
	  	} else {
  			res.send(JSON.stringify({"status": 200, "error": null, "response": results}));
	  	}
        connection.end();
  	});
});

/* GET latest approved delegates */
router.get('/latest/:limit', function(req, res, next) {
	const limit = [parseInt(req.params.limit)];
	connection.query('SELECT * from delegates WHERE approved = true ORDER BY id DESC LIMIT ?', limit, function (error, results, fields) {
	  	if(error){
	  		res.send(JSON.stringify({"status": 500, "error": error, "response": null})); 
	  	} else {
  			res.send(JSON.stringify({"status": 200, "error": null, "response": results}));
	  	}
        connection.end();
  	});
});

/* GET pending delegates */
router.get('/pending', function(req, res, next) {
	connection.query('SELECT * from delegates WHERE pending = true', function (error, results, fields) {
	  	if(error){
	  		res.send(JSON.stringify({"status": 500, "error": error, "response": null})); 
	  	} else {
  			res.send(JSON.stringify({"status": 200, "error": null, "response": results}));
	  	}
        connection.end();
  	});
});

/* GET rejected delegates */
router.get('/rejected', function(req, res, next) {
	connection.query('SELECT * from delegates WHERE rejected = true', function (error, results, fields) {
	  	if(error){
	  		res.send(JSON.stringify({"status": 500, "error": error, "response": null})); 
	  	} else {
  			res.send(JSON.stringify({"status": 200, "error": null, "response": results}));
	  	}
        connection.end();
  	});
});

/* POST delegate, tip */
router.post('/tip', function(req, res, next) {
    const id = [req.body.id];
    const tip = [req.body.tip];
    connection.query(`SET @update_id := ${id}; UPDATE delegates SET message = '${tip}' WHERE ID = @update_id; SELECT * from delegates WHERE ID = @update_id`, function (error, results, fields) {
        if(error){
            res.send(JSON.stringify({"status": 500, "error": error, "response": null}));
        } else {
            io.of('/').emit('app/TIP_UPDATED', {
                type: 'app/TIP_UPDATED',
            });
            res.send(JSON.stringify({"status": 200, "error": null, "response": results}));
        }
        connection.end();
    });
});

/* PATCH delegate, approve */
router.patch('/approve/:id', function(req, res, next) {
	const id = [req.params.id];
	connection.query(`SET @update_id := ${id}; UPDATE delegates SET approved = true, rejected = false, pending = false WHERE ID = @update_id; SELECT * from delegates WHERE ID = @update_id`, function (error, results, fields) {
	  	if(error){
	  		res.send(JSON.stringify({"status": 500, "error": error, "response": null})); 
	  	} else {
            io.of('/').emit('app/NEW_APPROVAL', {
                type: 'app/NEW_APPROVAL',
            });
  			res.send(JSON.stringify({"status": 200, "error": null, "response": results}));
	  	}
        connection.end();
  	});
});

/* DELETE delegate */
router.delete('/delete/:id', function(req, res, next) {
    const id = [req.params.id];
    connection.query(`SET @update_id := ${id}; SELECT ID from delegates WHERE ID = @update_id; DELETE FROM delegates WHERE id = @update_id`, function (error, results, fields) {
        if(error){
            res.send(JSON.stringify({"status": 500, "error": error, "response": null}));
        } else {
            io.of('/').emit('app/DELEGATE_DELETED', {
                type: 'app/DELEGATE_DELETED',
            });
            res.send(JSON.stringify({"status": 200, "error": null, "response": results}));
        }
        connection.end();
    });
});

/* PATCH delegate, reject */
router.patch('/reject/:id', function(req, res, next) {
	const id = [req.params.id];
	connection.query(`SET @update_id := ${id}; UPDATE delegates SET approved = false, rejected = true, pending = false WHERE ID = @update_id; SELECT * from delegates WHERE ID = @update_id`, function (error, results, fields) {
	  	if(error){
	  		res.send(JSON.stringify({"status": 500, "error": error, "response": null})); 
	  	} else {
  			res.send(JSON.stringify({"status": 200, "error": null, "response": results}));
	  	}
        connection.end();
  	});
});

/* PATCH delegate, reset */
router.patch('/reset/:id', function(req, res, next) {
    const id = [req.params.id];
    connection.query(`SET @update_id := ${id}; UPDATE delegates SET approved = false, rejected = false, pending = true WHERE ID = @update_id; SELECT * from delegates WHERE ID = @update_id`, function (error, results, fields) {
        if(error){
            res.send(JSON.stringify({"status": 500, "error": error, "response": null}));
        } else {
            io.of('/').emit('app/APPROVAL_REVOKED', {
                type: 'app/APPROVAL_REVOKED',
            });
            res.send(JSON.stringify({"status": 200, "error": null, "response": results}));
        }
        connection.end();
    });
});

module.exports = router;

//
