const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const mysql = require("mysql");

const { DATABASE_CONNECTION } = require('./config');

const indexRouter = require('./routes/index');
const delegateRouter = require('./routes/delegate');

let app = module.exports = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json({limit: '50mb'}));
app.use(express.urlencoded({limit: '50mb', extended: true}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

//Database connection
app.use(function(req, res, next){
	global.connection = mysql.createConnection(DATABASE_CONNECTION);
	connection.connect();
	next();
});
app.use('/', indexRouter);
app.use('/api/v1/delegate', delegateRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

// test connection

global.connection = mysql.createConnection(DATABASE_CONNECTION);
connection.connect();
connection.query('SELECT * from delegates WHERE approved = true ORDER BY id DESC LIMIT ?', 1, function (error, results) {
    if(error){
        console.log({"status": 500, "error": error, "response": null});
        connection.end();
    } else {
        console.log({"status": 200/*, "error": null, "response": results*/});
        connection.end();
    }
});

module.exports = app;
