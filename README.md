forever start --minUptime 1000 --spinSleepTime 1000 ./bin/www
forever list
forever stopall

DEBUG=wfh-api:* npm start

( DEBUG=wfh-api:* nohup npm start
tail -f nohup.out )

lsof -i:<PORT>
kill $(lsof -t -i:<PORT>)

ssh cslmay18@pegasus-stage.wealdnet.co.uk

